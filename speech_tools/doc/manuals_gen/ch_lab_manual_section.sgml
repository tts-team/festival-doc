  <sect1 id='ch-lab-manual'>
	<title><command>ch_lab</command> <emphasis>Label file manipulation</emphasis></title>

    <toc depth='1'></toc>
    <para>
    </para>
    <sect2>
      <title>Synopsis</title>
      <para>
      </para>
        <!-- /amd/projects/festival/versions/v_mpiro/speech_tools_linux/bin/ch_lab -sgml_synopsis -->
        <para>
<cmdsynopsis><command>ch_lab</command> [input file1] [input file2] -o [output file]<arg>-h </arg>
<arg>-S <replaceable>float</replaceable></arg>
<arg>-base </arg>
<arg>-class <replaceable>string</replaceable></arg>
<arg>-combine </arg>
<arg>-divide </arg>
<arg>-end <replaceable>float</replaceable></arg>
<arg>-ext <replaceable>string</replaceable></arg>
<arg>-extend <replaceable>float</replaceable></arg>
<arg>-extract <replaceable>string</replaceable></arg>
<arg>-f <replaceable>int</replaceable></arg>
<arg>-itype <replaceable>string</replaceable></arg>
<arg>-key <replaceable>string</replaceable></arg>
<arg>-lablist <replaceable>string</replaceable></arg>
<arg>-length <replaceable>float</replaceable></arg>
<arg>-lf <replaceable>int</replaceable></arg>
<arg>-map <replaceable>string</replaceable></arg>
<arg>-name <replaceable>string</replaceable></arg>
<arg>-nopath </arg>
<arg>-o <replaceable>ofile</replaceable></arg>
<arg>-off <replaceable>float</replaceable></arg>
<arg>-ops </arg>
<arg>-otype <replaceable>string</replaceable> " {esps}"</arg>
<arg>-pad <replaceable>string</replaceable></arg>
<arg>-pos <replaceable>string</replaceable></arg>
<arg>-q <replaceable>float</replaceable></arg>
<arg>-range <replaceable>float</replaceable></arg>
<arg>-sed <replaceable>ifile</replaceable></arg>
<arg>-shift <replaceable>float</replaceable></arg>
<arg>-start <replaceable>float</replaceable></arg>
<arg>-style <replaceable>string</replaceable></arg>
<arg>-vocab <replaceable>ifile</replaceable></arg>
<arg>-verify </arg>
</cmdsynopsis>
        </para>
        <!-- DONE /amd/projects/festival/versions/v_mpiro/speech_tools_linux/bin/ch_lab -sgml_synopsis -->
      <para>

ch_lab is used to manipulate the format of label files and 
serves as a wrap-around for the EST_Relation class.
      </para>
    </sect2>
    <sect2>
      <title>OPTIONS</title>
      <para>
      </para>
        <!-- /amd/projects/festival/versions/v_mpiro/speech_tools_linux/bin/ch_lab -sgml_options -->
        <para>
<variablelist>
<varlistentry><term>-h</term>
<LISTITEM><PARA>

Options help 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-S</term>
<LISTITEM><PARA>
<replaceable>float</replaceable>

frame spacing of output 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-base</term>
<LISTITEM><PARA>

use base filenames for lists of label files 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-class</term>
<LISTITEM><PARA>
<replaceable>string</replaceable>

Name of class defined in op file 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-combine</term>
<LISTITEM><PARA>

</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-divide</term>
<LISTITEM><PARA>

</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-end</term>
<LISTITEM><PARA>
<replaceable>float</replaceable>

end time (secs) for label extraction 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-ext</term>
<LISTITEM><PARA>
<replaceable>string</replaceable>

filename extension 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-extend</term>
<LISTITEM><PARA>
<replaceable>float</replaceable>

extend track file beyond label file 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-extract</term>
<LISTITEM><PARA>
<replaceable>string</replaceable>

extract a single file from a list of label files 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-f</term>
<LISTITEM><PARA>
<replaceable>int</replaceable>

sample frequency of label file 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-itype</term>
<LISTITEM><PARA>
<replaceable>string</replaceable>

type of input label file: esps htk ogi 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-key</term>
<LISTITEM><PARA>
<replaceable>string</replaceable>

key label file 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-lablist</term>
<LISTITEM><PARA>
<replaceable>string</replaceable>

list of labels to be considered as blank 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-length</term>
<LISTITEM><PARA>
<replaceable>float</replaceable>

length of track produced 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-lf</term>
<LISTITEM><PARA>
<replaceable>int</replaceable>

sample frequency for labels 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-map</term>
<LISTITEM><PARA>
<replaceable>string</replaceable>

name of file containing label mapping 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-name</term>
<LISTITEM><PARA>
<replaceable>string</replaceable>

eg. Fo Phoneme 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-nopath</term>
<LISTITEM><PARA>

ignore pathnames when searching label lists 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-o</term>
<LISTITEM><PARA>
<replaceable>ofile</replaceable>

output gile name 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-off</term>
<LISTITEM><PARA>
<replaceable>float</replaceable>

vertical offset of track 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-ops</term>
<LISTITEM><PARA>

print options 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-otype</term>
<LISTITEM><PARA>
<replaceable>string</replaceable>
 " {esps}"
output file type: xmg, ascii, esps, htk 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-pad</term>
<LISTITEM><PARA>
<replaceable>string</replaceable>

Pad with "high" or "low" values 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-pos</term>
<LISTITEM><PARA>
<replaceable>string</replaceable>

list of labels to be regarded as 'pos' 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-q</term>
<LISTITEM><PARA>
<replaceable>float</replaceable>

quantize label timings to nearest value 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-range</term>
<LISTITEM><PARA>
<replaceable>float</replaceable>

different between high and low values 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-sed</term>
<LISTITEM><PARA>
<replaceable>ifile</replaceable>

perform regex editing using sed file 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-shift</term>
<LISTITEM><PARA>
<replaceable>float</replaceable>

shift the times of the labels 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-start</term>
<LISTITEM><PARA>
<replaceable>float</replaceable>

start time for label extraction 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-style</term>
<LISTITEM><PARA>
<replaceable>string</replaceable>

output stype e.g. track 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-vocab</term>
<LISTITEM><PARA>
<replaceable>ifile</replaceable>

file containing list of words in vocab 
</PARA></LISTITEM>
</varlistentry>

<varlistentry><term>-verify</term>
<LISTITEM><PARA>

check that only labels in vocab file are in label file </PARA></LISTITEM>
</varlistentry>
</variablelist>
        </para>
        <!-- DONE /amd/projects/festival/versions/v_mpiro/speech_tools_linux/bin/ch_lab -sgml_options -->
    </sect2>
  </sect1>
